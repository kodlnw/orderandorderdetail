/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.mycompany.data.dao.OrderDao;
import com.mycompany.data.model.Orders_Detail;
import com.mycompany.data.model.Orders;
import com.mycompany.data.model.product;

/**
 *
 * @author roman
 */
public class testOrder {

    public static void main(String[] args) {
        product product1 = new product(1, "A", 75);
        product product2 = new product(2, "B", 180);
        product product3 = new product(3, "C", 70);
        Orders order = new Orders();
//        Orders_Detail orderDetail1 = new Orders_Detail(product1,product1.getName(),product1.getPrice(),1,order);
//        order.addOrders_Detail(orderDetail1);
        order.addOrders_Detail(product1, 10);
        order.addOrders_Detail(product2, 5);
        order.addOrders_Detail(product3, 3);
        System.out.println(order);
        System.out.println(order.getOrders_Details());
        OrdersDao ordersDao = new OrdersDao();
        Orders newOrders = ordersDao.save(order);
        System.out.println(newOrders);

        Orders order1 = ordersDao.get(newOrders.getId());
        printReciept(order1);
//        System.out.println(orderDao.get(3));
    }

    static void printReciept(Orders order) {
        System.out.println("Order" + order.getId());
        for (Orders_Detail od : order.getOrders_Details()) {
            System.out.println(" " + od.getproductName() + " " + od.getQty() + " " + od.getproductPrice() + " " + od.getTotal());
        }
        System.out.println("Total: " + order.getTotal() + "Qty: " + order.getQty());
    }
}

